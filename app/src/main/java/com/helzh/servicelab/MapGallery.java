package com.helzh.servicelab;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

/**
 * Created by 1 on 10.02.2018.
 */

@Entity
public class MapGallery {
    @PrimaryKey(autoGenerate = true) public int id;
    public double v1;
    public double v2;

    public MapGallery(double v1, double v2)
    {
        this.v1 = v1;
        this.v2= v2;
    }
}
