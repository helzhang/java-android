package com.helzh.servicelab;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

/**
 * Created by 1 on 10.02.2018.
 */

@Dao
public interface MapGalleryDAO {
    @Insert
    void insertAll(MapGallery... pictures);

    @Delete
    void delete(MapGallery gallery);

    @Query("SELECT * FROM mapgallery")
    List<MapGallery> getAll();
}
