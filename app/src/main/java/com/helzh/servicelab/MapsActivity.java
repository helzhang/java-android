package com.helzh.servicelab;

import android.Manifest;
import android.arch.persistence.room.Room;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.helzh.servicelab.services.LocationService;

import java.util.List;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener {

    private GoogleMap mMap;
    private static final String TAG = MapsActivity.class.getSimpleName();
    private Marker mMarkerPositionNow=null;
    AppDatabase db;

    private boolean mAlreadyStartedService = false;
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    //get coords from database
    private void getCoords()
    {
        new AsyncTask<Void, Void, List<MapGallery>>()
        {
            @Override
            protected List<MapGallery> doInBackground(Void... voids){
                return db.getMapGalleryDAO().getAll();
            }

            @Override
            protected void onPostExecute(List<MapGallery> mapgal)
            {
                for (int i = 0; i < mapgal.size(); i++)
                {
                    mMap.addMarker(new MarkerOptions().position(new LatLng(mapgal.get(i).v1, mapgal.get(i).v2)).title("Marker"));
                }
            }
        }.execute();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //if recieved a broadcast message from service
        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        double latitude = intent.getDoubleExtra(LocationService.EXTRA_LATITUDE, -200);
                        double longitude = intent.getDoubleExtra(LocationService.EXTRA_LONGITUDE, -200);

                        if (latitude != -200 && longitude != -200) {
                            if (mMarkerPositionNow != null) mMarkerPositionNow.remove();
                            mMarkerPositionNow = mMap.addMarker(new MarkerOptions().position(new LatLng(latitude, longitude)).title("Your location")
                                    .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

                        }
                    }
                }, new IntentFilter(LocationService.ACTION_LOCATION_BROADCAST)
        );
        db = Room.databaseBuilder(getApplicationContext(),AppDatabase.class, "service-database").build();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        getCoords();

    }

    @Override
    public void onMapClick(final LatLng latLng) {
        mMap.addMarker(new MarkerOptions().position(latLng).title("Marker"));
        new Thread(() -> {db.getMapGalleryDAO().insertAll(new MapGallery(latLng.latitude, latLng.longitude));}).start();
    }

    @Override
    public void onResume() {
        super.onResume();
        checkGooglePlay();
        if (mMarkerPositionNow!=null)  mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mMarkerPositionNow.getPosition(), 10));
    }


    /**
     * Step 1: Check Google Play services
     */
    private void checkGooglePlay() {

        //Check whether this user has installed Google play service which is being used by Location updates.
        if (isGooglePlayServicesAvailable()) {
            //Passing null to indicate that it is executing for the first time.
            checkNetworkAndPermissions(null);

        } else {
            Toast.makeText(getApplicationContext(),"Нет Google Play, пожалуйста, установите его", Toast.LENGTH_LONG).show();
        }
    }


    private Boolean checkNetworkAndPermissions(DialogInterface dialog) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if (activeNetworkInfo == null || !activeNetworkInfo.isConnected()) {
            promptInternetConnect();
            return false;
        }


        if (dialog != null) {
            dialog.dismiss();
        }

        //internet connection is active

        if (checkPermissions()) { //permissions granted
            startMyService();
        } else {  //no permissions, request it
            requestPermissions();
        }
        return true;
    }


    private void promptInternetConnect() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this);
        builder.setTitle("Нет интернета");
        builder.setMessage("Проверьте интернет соединение");

        String positiveText = "Перезагрузить";
        builder.setPositiveButton(positiveText,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        //Block the Application Execution until user grants the permissions
                        if (checkNetworkAndPermissions(dialog)) {

                            //Now make sure about location permission.
                            if (checkPermissions()) {
                                startMyService();
                            } else if (!checkPermissions()) {
                                requestPermissions();
                            }

                        }
                    }
                });

        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void startMyService() {

        if (!mAlreadyStartedService) {

            Intent intent = new Intent(this, LocationService.class);
            startService(intent);

            mAlreadyStartedService = true;
        }
    }


    //Return the availability of GooglePlayServices
    public boolean isGooglePlayServicesAvailable() {
        GoogleApiAvailability googleApiAvailability = GoogleApiAvailability.getInstance();
        int status = googleApiAvailability.isGooglePlayServicesAvailable(this);
        if (status != ConnectionResult.SUCCESS) {
            if (googleApiAvailability.isUserResolvableError(status)) {
                googleApiAvailability.getErrorDialog(this, status, 2404).show();
            }
            return false;
        }
        return true;
    }


    //Return true if all ok with permissions
    private boolean checkPermissions() {
        int permissionState1 = ActivityCompat.checkSelfPermission(this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);

        int permissionState2 = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION);

        return permissionState1 == PackageManager.PERMISSION_GRANTED && permissionState2 == PackageManager.PERMISSION_GRANTED;

    }

    //if no permissions request it
    private void requestPermissions() {

        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        android.Manifest.permission.ACCESS_FINE_LOCATION);

        boolean shouldProvideRationale2 =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_COARSE_LOCATION);

        if (shouldProvideRationale || shouldProvideRationale2) {
            showSnackbar("Пожалуйста, разрешите использовать геолокацию",
                    getString(android.R.string.ok), new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            // Request permission
                            ActivityCompat.requestPermissions(MapsActivity.this,
                                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_PERMISSIONS_REQUEST_CODE);
                        }
                    });
        } else {
            //ask again
            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }


    //fill and show control snackbar
    private void showSnackbar(String mainTextString, String actionString,
                              View.OnClickListener listener) {
        Snackbar.make(
                findViewById(android.R.id.content),
                mainTextString,
                Snackbar.LENGTH_INDEFINITE)
                .setAction(actionString, listener).show();
    }

    //what user asked
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                    //cancel
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //ok, granted
                startMyService();

            } else {
                showSnackbar("Вы не разрешили использовать геолокацию. Приложение не может работать",
                        "Настройки", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                // Build intent that displays the App settings screen.
                                Intent intent = new Intent();
                                intent.setAction(
                                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                                Uri uri = Uri.fromParts("package",
                                        BuildConfig.APPLICATION_ID, null);
                                intent.setData(uri);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
            }
        }
    }


    @Override
    public void onDestroy() {

        stopService(new Intent(this, LocationService.class));
        mAlreadyStartedService = false;

        super.onDestroy();
    }
}
