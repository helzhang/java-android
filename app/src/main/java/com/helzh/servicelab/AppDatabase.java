package com.helzh.servicelab;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

/**
 * Created by 1 on 10.02.2018.
 */
@Database(entities = {MapGallery.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase{
    public abstract MapGalleryDAO getMapGalleryDAO();
}

