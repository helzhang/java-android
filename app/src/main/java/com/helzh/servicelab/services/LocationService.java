package com.helzh.servicelab.services;

import android.Manifest;
import android.app.NotificationManager;
import android.app.Service;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.helzh.servicelab.AppDatabase;
import com.helzh.servicelab.MapGallery;
import com.helzh.servicelab.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by 1 on 20.03.2018.
 */

public class LocationService extends Service implements
        GoogleApiClient.ConnectionCallbacks,
        LocationListener {

    public static final int LOCATION_INTERVAL = 1000;
    public static final int FASTEST_LOCATION_INTERVAL = 500;

    GoogleApiClient mLocationClient;
    LocationRequest mLocationRequest = new LocationRequest();

    public static final String ACTION_LOCATION_BROADCAST = LocationService.class.getName() + "LocationBroadcast";
    public static final String EXTRA_LATITUDE = "extra_latitude";
    public static final String EXTRA_LONGITUDE = "extra_longitude";
    static final int Dist = 1000;

    AppDatabase db;
    ArrayList<MapGallery> list;

    private void getCoords()
    {
        new AsyncTask<Void, Void, List<MapGallery>>()
        {
            @Override
            protected List<MapGallery> doInBackground(Void... voids){
                return db.getMapGalleryDAO().getAll();
            }

            @Override
            protected void onPostExecute(List<MapGallery> mapgal) {
                list.addAll(mapgal);
            }
        }.execute();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mLocationClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .build();


        mLocationRequest.setInterval(LOCATION_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_LOCATION_INTERVAL);


        int priority = LocationRequest.PRIORITY_HIGH_ACCURACY;


        mLocationRequest.setPriority(priority);
        mLocationClient.connect();

        db = Room.databaseBuilder(getApplicationContext(),AppDatabase.class, "service-database").build();
        list = new ArrayList<MapGallery>();

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onConnected(Bundle dataBundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            //Permission not granted by user, do nothing
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mLocationClient, mLocationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    //to get the location change
    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            //Send result to activities and send notifications
            sendMessageToUI(location.getLatitude(), location.getLongitude());
            sendNotifications(location.getLatitude(), location.getLongitude());
        }

    }

    private void sendNotifications(double lat, double lng)
    {
        getCoords();
        if (!list.isEmpty())
        {
            NotificationManager mNotificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);

            NotificationCompat.Builder mNotifyBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle("Сообщение")
                    .setContentText("Message")
                    .setSmallIcon(R.drawable.ic_launcher_background);
            for (MapGallery item : list)
            {
                int notifyID = item.id;
                double distantion = CalcDist(lat, lng, item.v1, item.v2);
                if (distantion <= Dist)
                {
                    String text = "Вы находитесь на расстоянии " + (int)distantion + "м от точки";
                    mNotifyBuilder.setContentText(text);
                    mNotificationManager.notify(notifyID, mNotifyBuilder.build());
                }
            }
        }
    }

    //calc distance between two points
    double CalcDist(double lat1, double lng1, double lat2, double lng2)
    {
        float[] results = new float[1];
        Location.distanceBetween(lat1, lng1, lat2, lng2, results);
        return results[0];
    }

    //send message to activities
    private void sendMessageToUI(double lat, double lng) {
        Intent intent = new Intent(ACTION_LOCATION_BROADCAST);
        intent.putExtra(EXTRA_LATITUDE, lat);
        intent.putExtra(EXTRA_LONGITUDE, lng);
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
    }
}
